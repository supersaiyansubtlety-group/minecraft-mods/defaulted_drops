- 1.4.1 (4 Dec. 2024): Marked as compatible with 1.21.4
- 1.4.0 (24 Nov. 2024):
  - Updated for 1.21.2-1.21.3
  - Restructured build to be up to 80% less screwy or less
- 1.3.1 (10 Aug. 2024):
  - Marked as compatible with 1.21.1
  - Minor internal change
- 1.3.0 (13 Jun. 2024): Updated for 1.21
- 1.3.0-b1 (30 May 2024): Updated for 1.21-pre1
- 1.2.4 (8 May 2024):
  - Marked as compatible with 1.20.6
  - Increased minimum Minecraft version to 1.20.5
- 1.2.3 (23 Apr. 2024): Marked as compatible with 1.20.5
- 1.2.3-b1unbounded (22 Apr. 2024): Same as 1.2.3-b1 without upper bounds on dependencies in fmj
- 1.2.3-b1 (22 Apr. 2024): Updated for 1.20.5-rc2
- 1.2.2 (26 Jan. 2024): Marked as compatible with 1.20.3 and 1.20.4
- 1.2.1 (26 Nov. 2023): Updated for 1.20.2
- 1.2.0 (28 Jun. 2023): Blocks tagged with "defaulted_drops:needs_silk_touch" will now only drop if they were broken
using a tool with silk touch.
- 1.1.0 (27 Jun. 2023):
  - ***NOTICE***: If you use this library in your mod, please let me know; whether it's on 
[discord](https://discord.gg/xABmPngXAH), 
[CurseForge](https://legacy.curseforge.com/members/supersaiyansubtlety), or even in an 
[issue](https://gitlab.com/supersaiyansubtlety-group/minecraft-mods/defaulted_drops/-/issues) on the repo!
I have no indication that this library sees actual use, so I'm considering leaving it on 1.20 if I still don't know
it's seeing use when 1.21 releases.
  - Updated for 1.20 and 1.20.1!
  - Added special case for slabs: double slabs will drop two items
  - Added special case for two-tall blocks: only the bottom half of two-tall blocks will drop an item
  - Added "survives_explosion" check for all defaulted drops
  - Thanks to [Siuol](https://github.com/Siuolplex) for the idea of adding special cases for slabs and two-tall blocks!
- 1.0.5 (4 Apr. 2023): Marked as compatible with 1.19.3 and 1.19.4
- 1.0.4 (7 Aug. 2022): Marked as compatible with 1.19.2
- 1.0.3 (29 Jul. 2022): Marked as compatible with 1.19.1
- 1.0.2 (28 Jun. 2022): Updated for 1.19!
- 1.0.1 (29 May 2022): Release version for 1.18.2
- 1.0.1-a1 (29 May 2022): Alpha version for 1.18.2
- 1.0.0 (15 Jan. 2022): First release version!
- 0.0.4 (15 Jan. 2022): Added crucial null check when looking for custom values, made initialization time consistent.
- 0.0.3 (15 Jan. 2022): Now looks for custom values in `fabric.mod.json`s.
- 0.0.2 (15 Jan. 2022): Testing with test mod.
- 0.0.1 (14 Jan. 2022): Renamed things from template mod.
