package net.sssubtlety.defaulted_drops;

import net.minecraft.block.Block;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.enchantment.Enchantments;
import net.minecraft.item.ItemStack;
import net.minecraft.predicate.NumberRange;
import net.minecraft.predicate.item.EnchantmentPredicate;
import net.minecraft.registry.RegistryKeys;
import net.minecraft.registry.tag.TagKey;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.Identifier;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Objects;

public class DefaultedDrops {
	public static final String NAMESPACE = "defaulted_drops";

	public static final Logger LOGGER = LoggerFactory.getLogger(NAMESPACE);

	public static final TagKey<Block> NEEDS_SILK_TOUCH =
		TagKey.of(RegistryKeys.BLOCK, Identifier.of(NAMESPACE, "needs_silk_touch"));

	/**
	 * @throws NullPointerException if {@value NAMESPACE} hasn't finished {@linkplain Init#onInitialize() initializing}
	 */
	public static boolean isOptedIn(String namespace) {
		return Objects.requireNonNull(
			Init.optedInNamespaces,
			NAMESPACE + " not yet initialized!"
		).contains(namespace);
	}

	public static boolean lacksSilkTouch(@Nullable ItemStack tool, ServerWorld world) {
		if (tool == null) {
			return false;
		}

		return world.getRegistryManager().getLookup(RegistryKeys.ENCHANTMENT)
			.flatMap(enchantments -> enchantments.getHolder(Enchantments.SILK_TOUCH))
			.map(silkTouch -> new EnchantmentPredicate(silkTouch, NumberRange.IntRange.ANY))
			.filter(silkTouchPredicate -> silkTouchPredicate.test(EnchantmentHelper.getEnchantments(tool)))
			.isEmpty();
	}
}
