package net.sssubtlety.defaulted_drops;

import com.google.common.collect.ImmutableSet;
import org.jetbrains.annotations.Nullable;

import net.fabricmc.api.ModInitializer;
import net.fabricmc.loader.api.FabricLoader;
import net.fabricmc.loader.api.ModContainer;
import net.fabricmc.loader.api.metadata.CustomValue;
import net.fabricmc.loader.api.metadata.ModMetadata;

import java.util.HashSet;
import java.util.Set;

import static net.sssubtlety.defaulted_drops.DefaultedDrops.LOGGER;
import static net.sssubtlety.defaulted_drops.DefaultedDrops.NAMESPACE;

import static net.fabricmc.loader.api.metadata.CustomValue.CvType.ARRAY;
import static net.fabricmc.loader.api.metadata.CustomValue.CvType.BOOLEAN;
import static net.fabricmc.loader.api.metadata.CustomValue.CvType.STRING;

public class Init implements ModInitializer {
    @Nullable
    static ImmutableSet<String> optedInNamespaces;

    @Override
    public void onInitialize () {
        final Set<String> namespaceSetBuilder = new HashSet<>();
        for (final ModContainer container : FabricLoader.getInstance().getAllMods()) {
            final ModMetadata metadata = container.getMetadata();
            final CustomValue customValue = metadata.getCustomValue(NAMESPACE);
            if (customValue == null) {
                continue;
            }

            final CustomValue.CvType type = customValue.getType();
            switch (type) {
                case BOOLEAN -> {
                    if (customValue.getAsBoolean()) {
                        namespaceSetBuilder.add(metadata.getId());
                    }
                }
                case STRING -> namespaceSetBuilder.add(customValue.getAsString());
                case ARRAY -> {
                    final CustomValue.CvArray array = customValue.getAsArray();
                    for (final CustomValue element : array) {
                        final CustomValue.CvType elementType = element.getType();
                        if (elementType == STRING) {
                            namespaceSetBuilder.add(element.getAsString());
                        } else {
                            LOGGER.error(
                                """
                                Found invalid type within array value for custom "{}" \
                                field in fabric.mod.json for mod: "{}"!
                                Array must only contain {} values; found {}
                                """,
                                NAMESPACE,
                                metadata.getId(),
                                STRING, elementType
                            );
                        }
                    }
                }
                default -> LOGGER.error(
                    """
                    Found invalid value type for custom "{}" field in fabric.mod.json for mod: "{}"!
                    Expected {}, {}, or {}; found {}
                    """,
                    NAMESPACE, metadata.getId(),
                    BOOLEAN, STRING, ARRAY, type
                );
            }
        }

        optedInNamespaces = ImmutableSet.copyOf(namespaceSetBuilder);
    }
}
