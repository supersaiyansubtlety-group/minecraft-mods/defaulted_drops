package net.sssubtlety.defaulted_drops.mixin;

import com.llamalad7.mixinextras.injector.wrapoperation.Operation;
import com.llamalad7.mixinextras.injector.wrapoperation.WrapOperation;

import net.minecraft.block.AbstractBlock;
import net.minecraft.block.BlockState;
import net.minecraft.block.enums.DoubleBlockHalf;
import net.minecraft.block.enums.SlabType;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.loot.LootTable;
import net.minecraft.loot.condition.LootCondition;
import net.minecraft.loot.condition.SurvivesExplosionLootCondition;
import net.minecraft.loot.context.LootContext;
import net.minecraft.loot.context.LootContextParameterSet;
import net.minecraft.loot.context.LootContextParameters;
import net.minecraft.registry.RegistryKey;
import net.minecraft.state.property.Properties;
import net.minecraft.util.Identifier;

import net.sssubtlety.defaulted_drops.DefaultedDrops;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;

import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.At;

import java.util.Optional;

import static net.sssubtlety.defaulted_drops.DefaultedDrops.lacksSilkTouch;

@Mixin(AbstractBlock.class)
public abstract class AbstractBlockMixin {
    @Unique
    private static final LootCondition SURVIVES_EXPLOSION_CONDITION = SurvivesExplosionLootCondition.builder().build();

    @Shadow
    public abstract Item asItem();

    @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
    @Shadow
    @Final
    protected Optional<RegistryKey<LootTable>> lootTableId;

    // NOTE: mineable/<tool> and needs_<material>_tool tags are checked before getDroppedStacks is called
    @WrapOperation(
        method = "getDroppedStacks",
        at = @At(
            value = "INVOKE",
            target = "Lnet/minecraft/loot/LootTable;generateLoot(" +
                "Lnet/minecraft/loot/context/LootContextParameterSet;)" +
                "Lit/unimi/dsi/fastutil/objects/ObjectArrayList;"
        )
    )
    private ObjectArrayList<ItemStack> addSelfIfNoLootTable(
        LootTable lootTable, LootContextParameterSet parameterSet, Operation<ObjectArrayList<ItemStack>> original,
        BlockState state, LootContextParameterSet. Builder lootParameterBuilder
    ) {
        return this.tryAddSelfIfNoLootTable(state, lootTable, parameterSet)
            .orElse(original.call(lootTable, parameterSet));
    }

    @Unique
    private Optional<ObjectArrayList<ItemStack>> tryAddSelfIfNoLootTable(
        BlockState selfState, LootTable lootTable, LootContextParameterSet parameterSet
    ) {
        if (!this.shouldAddSelf(selfState, lootTable, parameterSet)) {
            return Optional.empty();
        }

        return Optional.ofNullable(this.asItem())
            .flatMap(item -> Optional.ofNullable(item.getDefaultStack()))
            .map(stack -> {
                // drop two if it's a double slab
                if (selfState.getOrEmpty(Properties.SLAB_TYPE).filter(type -> type == SlabType.DOUBLE).isPresent()) {
                    stack.setCount(2);
                }

                return stack;
            })
            .map(ObjectArrayList::of);
    }

    @Unique
    private boolean shouldAddSelf(BlockState selfState, LootTable lootTable, LootContextParameterSet parameterSet) {
        if (
            lootTable == LootTable.EMPTY &&
                this.lootTableId
                    .map(RegistryKey::getValue)
                    .map(Identifier::getNamespace)
                    .filter(DefaultedDrops::isOptedIn)
                    .isPresent()
        ) {
            if (selfState == null) {
                return false;
            }

            final LootContext lootContext = new LootContext.Builder(parameterSet).build(Optional.empty());
            if (!SURVIVES_EXPLOSION_CONDITION.test(lootContext)) {
                return false;
            }

            // don't drop items for the top half of double blocks, like vanilla LootTables
            if (
                selfState.getOrEmpty(Properties.DOUBLE_BLOCK_HALF)
                    .filter(half -> half == DoubleBlockHalf.UPPER)
                    .isPresent()
            ) {
                return false;
            }

            // don't drop items for blocks that require silk touch if silk touch wasn't used to break them
            if (selfState.isIn(DefaultedDrops.NEEDS_SILK_TOUCH)) {
                //noinspection RedundantIfStatement
                if (lacksSilkTouch(lootContext.get(LootContextParameters.TOOL), parameterSet.getWorld())) {
                    return false;
                }
            }

            return true;
        } else {
            return false;
        }
    }
}
