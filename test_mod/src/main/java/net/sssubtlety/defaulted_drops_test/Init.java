package net.sssubtlety.defaulted_drops_test;

import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.itemgroup.v1.ItemGroupEvents;

import net.minecraft.block.AbstractBlock.Settings;
import net.minecraft.block.Block;
import net.minecraft.block.BlockSetType;
import net.minecraft.block.Blocks;
import net.minecraft.block.DoorBlock;
import net.minecraft.block.SlabBlock;
import net.minecraft.block.TallPlantBlock;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroups;
import net.minecraft.registry.Registries;
import net.minecraft.registry.Registry;
import net.minecraft.registry.RegistryKey;
import net.minecraft.registry.RegistryKeys;

import java.util.function.Function;
import java.util.function.UnaryOperator;

public class Init implements ModInitializer {
    @Override
    public void onInitialize() {
        final Block basicBlock = registerBlock("basic", Block::new);
        final BlockItem basicItem = registerItem(basicBlock);

        final DoorBlock doorBlock = registerBlock("door", settings ->
            new DoorBlock(new BlockSetType(DefaultedDropsTest.NAMESPACE), settings)
        );
        final BlockItem doorItem = registerItem(doorBlock);

        final TallPlantBlock tallPlantBlock = registerBlock("tall_plant", TallPlantBlock::new);
        final BlockItem tallPlantItem = registerItem(tallPlantBlock);

        final SlabBlock slabBlock = registerBlock("slab", SlabBlock::new);
        final BlockItem slabItem = registerItem(slabBlock);

        final Block needsSilkTouchBlock = registerBlock("needs_silk_touch", Block::new);
        final BlockItem needsSilkTouchItem = registerItem(needsSilkTouchBlock);

        final Block needsPickaxeBlock = registerBlock("needs_pickaxe", Block::new, Settings::toolRequired);
        final BlockItem needsPickaxeItem = registerItem(needsPickaxeBlock);

        final Block needsIronAxeBlock = registerBlock("needs_iron_axe", Block::new, Settings::toolRequired);
        final BlockItem needsIronAxeItem = registerItem(needsIronAxeBlock);

        ItemGroupEvents.modifyEntriesEvent(ItemGroups.OPERATOR_UTILITIES).register(entries -> {
            entries.addItem(basicItem);
            entries.addItem(doorItem);
            entries.addItem(tallPlantItem);
            entries.addItem(slabItem);
            entries.addItem(needsSilkTouchItem);
            entries.addItem(needsPickaxeItem);
            entries.addItem(needsIronAxeItem);
        });
    }

    private static <B extends Block> B registerBlock(String path, Function<Settings, B> factory) {
        return registerBlock(path, factory, UnaryOperator.identity());
    }

    private static <B extends Block> B registerBlock(
        String path, Function<Settings, B> factory, UnaryOperator<Settings> configuration
    ) {
        final RegistryKey<Block> key = RegistryKey.of(RegistryKeys.BLOCK, DefaultedDropsTest.idOf(path));

        return Registry.register(
            Registries.BLOCK, key,
            factory.apply(configuration.apply(Settings.copy(Blocks.TNT)).key(key))
        );
    }

    private static BlockItem registerItem(Block block) {
        @SuppressWarnings("deprecation")
        final RegistryKey<Item> key =
            RegistryKey.of(RegistryKeys.ITEM, block.getBuiltInRegistryHolder().getRegistryKey().getValue());

        final BlockItem blockItem = Registry.register(
            Registries.ITEM, key,
            new BlockItem(block, new Item.Settings().key(key).blockTranslationKey())
        );

        Item.BLOCK_ITEMS.put(block, blockItem);

        return blockItem;
    }
}
