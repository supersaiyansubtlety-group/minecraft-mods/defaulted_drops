package net.sssubtlety.defaulted_drops_test;

import net.minecraft.util.Identifier;

public class DefaultedDropsTest {
	public static final String NAMESPACE = "defaulted_drops_test";

	public static Identifier idOf(String path) {
		return Identifier.of(NAMESPACE, path);
	}
}
